
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.base, name="base"),
    url(r'delete', views.delete, name="delete"),
    url(r'trashdetails/(?P<path_to_trash>.+)', views.trash_details, name="trash_details"),
    url(r'recover/(?P<item>[\d]+)/(?P<path_to_trash>.+)', views.recover, name="recover")
]
