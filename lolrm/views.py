# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, HttpResponse
from models import Trash
from conslolrm.trash import setup_trash, save_trash_metadata
from conslolrm.args import parsing_arguments
from conslolrm.setting import setup_utility
from conslolrm.logger import  setup_logging
from conslolrm.delete import delete_directory, delete_file_to_trash
from conslolrm.mode import recover_items_by_id
import threading
import os

def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

@singleton
class TaskInfo():
    def __init__(self):
        self._tasks = []

    def add_task(self, task):
        self._tasks.append(task)

    def get_tasks(self):
        return  self._tasks


class DeleteItemTask(threading.Thread):
    def __init__(self, trash_items, path_to_trash, item, config, level=0):
        threading.Thread.__init__(self)
        self._path_to_trash = path_to_trash
        self._item = item
        self._sub_tasks = []
        self._trash_metadata = trash_items
        self._config = config
        self._level = level

    def run(self):
        if os.path.isfile(self._item):
            self._delete_file(self._item)
        elif os.path.isdir(self._item):
            self._delete_dir(self._item)

        for task in self._sub_tasks:
            task.join()
            for item in task.get_trash_metadata():
                self._trash_metadata.append(item)

        if os.path.isdir(self._item):
            self._delete_root()
        if self._level == 0:
            save_trash_metadata(self._config['trash'], self._trash_metadata)


    def _delete_root(self):
        self._trash_metadata = delete_directory(self._item, self._path_to_trash, trash_metadata=self._trash_metadata)

    def _delete_file(self, item):
        self._trash_metadata = delete_file_to_trash(item, self._path_to_trash, trash_metadata=self._trash_metadata)

    def _delete_dir(self, item):
        for path_item in os.listdir(item):
            item_path = os.path.join(item, path_item)
            if os.path.isfile(item_path):
                self._delete_file(item_path)
            elif os.path.isdir(item_path):
                task = DeleteItemTask([], self._path_to_trash, item_path, self._config, self._level+1)
                self._sub_tasks.append(task)
                TaskInfo().add_task(task)
                task.start()

    def get_trash_metadata(self):
        return self._trash_metadata

# Create your views here.
def base(request):
    trash = Trash.objects.all()

    recover_items_by_id()
    property = {
        'trash': trash
    }

    return render(request, "base.html", property)

def recover(request, item, path_to_trash):
    trash = Trash.objects.all()

    args = parsing_arguments("-p {path}".format(path=path_to_trash))
    config = setup_utility(args)
    setup_logging(config['logger'], config['mode'])
    trash_items = setup_trash(config['trash'])
    trash_items = recover_items_by_id([item], path_to_trash, trash_items)
    save_trash_metadata(config['trash'], trash_items)

    property = {
        'trash': trash
    }
    return render(request, "base.html", property)


def delete(request):
    trash = Trash.objects.all()
    if request.method == 'POST':
        args = parsing_arguments("-p {path}".format(path=request.POST['trash']))
        config = setup_utility(args)
        trash_items = setup_trash(config['trash'])
        setup_logging(config['logger'], config['mode'])

        trash_task = DeleteItemTask(trash_items, request.POST['trash'], request.POST['path'], config)
        TaskInfo().add_task(trash_task)
        trash_task.start()

    property = {
        'trash': trash
    }
    return render(request, "base.html", property)

def trash_details(request, path_to_trash):
    trash = Trash.objects.all()
    selected_trash = Trash.objects.filter(path=path_to_trash)

    args = parsing_arguments("-p {path}".format(path=path_to_trash))
    config = setup_utility(args)
    trash_items = setup_trash(config['trash'])
    items_in_trash = []

    i = 0
    for item in trash_items:
        item = item.split(' | ')
        items_in_trash.append([item[1], item[2], {'id': i}])
        i += 1

    property = {
        'trash': trash,
        'trash_items': items_in_trash,
        'selected_trash': selected_trash,
    }
    return render(request, "base.html", property)