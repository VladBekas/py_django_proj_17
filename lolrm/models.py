# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Trash(models.Model):
    path = models.CharField(max_length=256)
    max_file_count = models.IntegerField()
    size = models.BigIntegerField()
    follow_links = models.BooleanField()
